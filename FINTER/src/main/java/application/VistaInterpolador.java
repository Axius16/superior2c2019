package application;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class VistaInterpolador extends Application{
	
	@Override
	public void start(Stage primaryStage) throws Exception{
	Parent root = FXMLLoader.load(getClass().getResource("/interpoladorNuevo.fxml"));	
	primaryStage.setScene(new Scene(root));
	primaryStage.getIcons().add(new Image("loguito.JPG"));
	primaryStage.setTitle("Calcular Polinomio Interpolador - Trabajo Practico Matematica Superior 2C-2019");
	primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	
}
