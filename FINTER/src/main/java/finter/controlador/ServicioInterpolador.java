package finter.controlador;

import finter.modelo.CalculadoraPolinomios;
import finter.modelo.Interpolador;
import finter.modelo.Polinomio;
import finter.modelo.ResultadoInterpolacion;
import javafx.util.Pair;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

public class ServicioInterpolador {

    public Boolean validarSiTieneValoresXRepetidos(ArrayList<Pair<Double, Double>> valoresIngresados){
        Set valoresXFiltrados = valoresIngresados.stream().map(Pair::getKey).collect(Collectors.toSet());
        return valoresXFiltrados.size() != valoresIngresados.size();
    }

    public ResultadoInterpolacion ejecutarInterpolacion(ArrayList<Pair<Double, Double>> valoresIngresados, String metodoAUtilizar){
        Interpolador interpolador = new Interpolador(valoresIngresados);
        ResultadoInterpolacion resultado = new ResultadoInterpolacion();

        Polinomio polinomioInterpolante = interpolador.obtenerPolinomioInterpolante();
        resultado.setGrado(String.valueOf(polinomioInterpolante.getGrado()));
        resultado.setLosPuntosSonEquidistantes(interpolador.susPuntosSonEquidistantes() ? "SI" : "NO");
        resultado.setPolinomioExtendido(polinomioInterpolante.toString());
        resultado.setPolinomio(interpolador.obtenerPolinomioInterpolante());

        String polinomioResultante;
        String desarrolloRealizado;
        if(metodoAUtilizar.equalsIgnoreCase("Lagrange")){
            polinomioResultante = interpolador.obtenerDesarrolloLagrange().obtenerPolinomioDeLagrange();
            desarrolloRealizado = interpolador.obtenerDesarrolloLagrange().obtenerDetalle();
        }
        else if(metodoAUtilizar.equalsIgnoreCase("Newton Gregory Progresivo")){
            polinomioResultante = interpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioProgresivo();
            desarrolloRealizado = interpolador.obtenerDesarrolloNewtonGregory().obtenerDetalle();
        }
        else if(metodoAUtilizar.equalsIgnoreCase("Newton Gregory Regresivo")){
            polinomioResultante = interpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioRegresivo();
            desarrolloRealizado = interpolador.obtenerDesarrolloNewtonGregory().obtenerDetalle();
        }
        else{
            throw new RuntimeException("El metodo seleccionado no es valido: " + metodoAUtilizar);
        }
        resultado.setPolinomioResultante(polinomioResultante);
        resultado.setDesarrolloDelMetodoRealizado(desarrolloRealizado);
        return resultado;
    }

    public String calcularValorEnK(Polinomio polinomio, Double k){
        Double valorCalcuado = CalculadoraPolinomios.calcularValorDeY(polinomio, k);
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);
        return df.format(valorCalcuado).replace(",", ".");
    }

    public Boolean esIgualAlAnterior(Polinomio polinomioAnterior, ArrayList<Pair<Double, Double>> valoresIngresados){
        Interpolador interpolador = new Interpolador(valoresIngresados);

        Polinomio nuevoPolinomio = interpolador.obtenerPolinomioInterpolante();
        return nuevoPolinomio.equals(polinomioAnterior);
    }
}
