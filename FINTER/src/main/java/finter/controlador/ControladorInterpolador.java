package finter.controlador;

import finter.modelo.Polinomio;
import finter.modelo.Punto2D;
import finter.modelo.ResultadoInterpolacion;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.util.Pair;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ControladorInterpolador implements Initializable {

    private ObservableList<Punto2D> puntos;
    private ObservableList<String> listaCombo = FXCollections.observableArrayList("Lagrange", "Newton Gregory Progresivo", "Newton Gregory Regresivo");

    private String metodo;
    private Polinomio polinomioActual;

    @FXML private TextField valorX;
    @FXML private TextField valorY;
    @FXML private TextField puntosEquidistantes;
    @FXML private TextField gradoDelPolinomio;
    @FXML private TextField valork;
    @FXML private TextField valorPolK;
    @FXML private TextArea polinomioResultante;
    @FXML private TextArea pasosDeCalculo;
    @FXML private Button modificarBtn;
    @FXML private Button eliminarBtn;
    @FXML private Button calcularBtn;
    @FXML private Button mostrarBtn;
    @FXML private Button compararBtn;
    @FXML private Button finalizar;
    @FXML private Button calcularPk;
    @FXML private TableView<Punto2D> tablaPuntos;
    @FXML private TableColumn valorXCol;
    @FXML private TableColumn valorYCol;
    @FXML private ComboBox<String> comboMetodo;
    @FXML private Label gradoLbl;
    @FXML private Label puntosLbl;
    @FXML private Label pasosCalculo;
    @FXML private Label esIgual;
    @FXML private Label polRes;
    @FXML private Label especializarPolinomio;
    @FXML private Label valorpLbl;
    @FXML private Label notifError;

    private int posicionEnTabla;

    @FXML
    private void calcular(ActionEvent event) {

        ArrayList<Pair<Double, Double>> listaDobles = convertirListaObservable(puntos);
        esIgual.setVisible(false);
        notifError.setText("");

        if (listaDobles.size() < 2) {
            //envia error al dejar uno de los campos vacios
            notifError.setText("ERROR: Debe ingresar al menos dos pares de valores para poder calcular el polinomio interpolador");
            return;
        }

        ServicioInterpolador servicio = new ServicioInterpolador();

        if (servicio.validarSiTieneValoresXRepetidos(listaDobles)) {
            notifError.setText("ERROR: No pueden existir dos valores iguales de X");
            return;
        }

        if (comboMetodo.getValue() == null) {
            notifError.setText("ERROR: Debe elegir un metodo de interpolacion");
            return;
        }
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(listaDobles, metodo);
        puntosEquidistantes.setText(resultado.getLosPuntosSonEquidistantes());
        polinomioResultante.setText(resultado.getPolinomioExtendido());
        gradoDelPolinomio.setText(resultado.getGrado());
        polinomioActual = resultado.getPolinomio();
        pasosDeCalculo.setText(
                "Desarrollo de " + metodo + ":\n" +
                resultado.getDesarrolloDelMetodoRealizado() + "\n" +
                "Polinomio de " + metodo + ":\n " +
                resultado.getPolinomioResultante());

        gradoDelPolinomio.setVisible(true);
        gradoLbl.setVisible(true);
        puntosLbl.setVisible(true);
        puntosEquidistantes.setVisible(true);
        polRes.setVisible(true);
        polinomioResultante.setVisible(true);
        especializarPolinomio.setVisible(true);
        valork.setVisible(true);
        valork.setText("");
        calcularPk.setVisible(true);
        valorpLbl.setVisible(true);
        valorPolK.setVisible(true);

        mostrarBtn.setDisable(false);
        compararBtn.setDisable(false);
        finalizar.setDisable(false);
        finalizar.setVisible(true);

    }

    @FXML
    private void mostrarPasos(ActionEvent event) {
        notifError.setText("");
        pasosDeCalculo.setVisible(true);
        pasosCalculo.setVisible(true);
    }

    @FXML
    private void comparar(ActionEvent event) {
        notifError.setText("");
        ServicioInterpolador servicio = new ServicioInterpolador();
        if(servicio.esIgualAlAnterior(polinomioActual, convertirListaObservable(puntos))){
            esIgual.setVisible(true);
            esIgual.setText("El polinomio resultante es igual al anterior.");
        }
        else{
            esIgual.setVisible(true);
            esIgual.setText("El polinomio resultante es distinto al anterior.");
        }
    }

    //Borra todos los datos y deja todo en el estado inicial.
    @FXML
    private void finalizar(ActionEvent event) {
        notifError.setText("");
        valorX.setText("");
        valorY.setText("");
        esIgual.setVisible(false);
        this.inicializarTablaPuntos();
        modificarBtn.setDisable(true);
        eliminarBtn.setDisable(true);
        calcularBtn.setDisable(true);
        mostrarBtn.setDisable(true);
        compararBtn.setDisable(true);
        comboMetodo.setValue(null);
        comboMetodo.setItems(listaCombo);

        gradoDelPolinomio.setVisible(false);
        gradoLbl.setVisible(false);
        pasosCalculo.setVisible(false);
        puntosLbl.setVisible(false);
        puntosEquidistantes.setVisible(false);
        esIgual.setVisible(false);
        polRes.setVisible(false);
        polinomioResultante.setVisible(false);
        pasosDeCalculo.setVisible(false);
        especializarPolinomio.setVisible(false);
        valork.setVisible(false);
        calcularPk.setVisible(false);
        valorpLbl.setVisible(false);
        valorPolK.setVisible(false);
    }

    //calcula el polinomio en el k especificado.
    @FXML
    private void calcularPk(ActionEvent event) {
        notifError.setText("");
        if (valork.getText() == null || valork.getText().trim().isEmpty()) {
            notifError.setText("ERROR: Debe ingresar un valor de K");
            return;
        }
        double k;
        try {
            k = Double.parseDouble(valork.getText().replace(",", "."));
        } catch (NumberFormatException e) {
            notifError.setText("ERROR: K debe ser numerico");
            return;
        }
        ServicioInterpolador servicio = new ServicioInterpolador();
        valorPolK.setText(servicio.calcularValorEnK(polinomioActual, k));
    }

    //Agrega un punto a la tabla
    @FXML
    private void aniadir(ActionEvent event) {
        notifError.setText("");

        if (valorX.getText().isEmpty() || valorY.getText().isEmpty()) {
            //envia error al dejar uno de los campos vacios
            notifError.setText("ERROR: Los campos X e Y NO pueden estar vacios");
            return;
        }

        double x;
        double y;
        try {
            x = Double.parseDouble(valorX.getText().replace(",", "."));
            y = Double.parseDouble(valorY.getText().replace(",", "."));
        } catch (NumberFormatException e) {
            //envia error al poner un valor NO numerico.
            notifError.setText("ERROR: X e Y deben tener valores numericos");
            return;
        }
        Punto2D nuevoPunto = new Punto2D(x, y);
        //if(nuevoPunto.valorX 0)
        puntos.add(nuevoPunto);
        valorX.setText("");
        valorY.setText("");

        calcularBtn.setDisable(false);
    }

    //Modifica un punto de la tabla
    @FXML
    private void modificar(ActionEvent event) {
        Double x = Double.parseDouble(valorX.getText());
        Double y = Double.parseDouble(valorY.getText());
        Punto2D nuevoPunto = new Punto2D(x, y);
        puntos.set(posicionEnTabla, nuevoPunto);
    }

    //Elimina un punto a la tabla
    @FXML
    private void eliminar(ActionEvent event) {
        puntos.remove(posicionEnTabla);
    }

    //Para seleccionar una fila de la tabla
    private final ListChangeListener<Punto2D> selectorTablaPuntos = c -> ponerPuntoSeleccionado();

    //Elije un punto de la tabla
    private Punto2D getTablaPuntoSeleccionado() {
        if (tablaPuntos != null) {
            List<Punto2D> tabla = tablaPuntos.getSelectionModel().getSelectedItems();
            if (tabla.size() == 1) {
                return tabla.get(0);
            }
        }
        return null;
    }

    private void ponerPuntoSeleccionado() {
        final Punto2D punto = getTablaPuntoSeleccionado();
        posicionEnTabla = puntos.indexOf(punto);

        if (punto != null) {
            //Actualizo los TextFields
            valorX.setText(punto.getValorX().toString());
            valorY.setText(punto.getValorY().toString());

            //Pongo los botones en el estado correspondiente
            modificarBtn.setDisable(false);
            eliminarBtn.setDisable(false);
        }
    }

    private void inicializarTablaPuntos() {
        valorXCol.setCellValueFactory(new PropertyValueFactory<Punto2D, Double>("valorX"));
        valorYCol.setCellValueFactory(new PropertyValueFactory<Punto2D, Double>("valorY"));

        puntos = FXCollections.observableArrayList();
        tablaPuntos.setItems(puntos);
    }

    // Convierte la tabla de Puntos en un Array de Dobles
    private ArrayList<Pair<Double, Double>> convertirListaObservable(ObservableList<Punto2D> puntos) {
        ArrayList<Pair<Double, Double>> nuevaLista = new ArrayList<>();

        for (Punto2D punto : puntos) {
            Pair<Double, Double> unPunto = new Pair<>(punto.getValorX(), punto.getValorY());
            nuevaLista.add(unPunto);
        }
        return nuevaLista;
    }


    public void comboMetodo(ActionEvent event) {
        metodo = comboMetodo.getValue();
    }

    //Este metodo se ejecuta al arrancar el programa
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.inicializarTablaPuntos();

        comboMetodo.setItems(listaCombo);

        polinomioResultante.setEditable(false);
        gradoDelPolinomio.setEditable(false);
        puntosEquidistantes.setEditable(false);
        valorPolK.setEditable(false);

        comboMetodo.setDisable(false);
        modificarBtn.setDisable(true);
        eliminarBtn.setDisable(true);
        calcularBtn.setDisable(true);
        mostrarBtn.setDisable(true);
        compararBtn.setDisable(true);
        finalizar.setDisable(false);

        finalizar.setVisible(true);
        gradoDelPolinomio.setVisible(false);
        gradoLbl.setVisible(false);
        pasosCalculo.setVisible(false);
        puntosLbl.setVisible(false);
        puntosEquidistantes.setVisible(false);
        esIgual.setVisible(false);
        polRes.setVisible(false);
        polinomioResultante.setVisible(false);
        pasosDeCalculo.setVisible(false);
        especializarPolinomio.setVisible(false);
        valork.setVisible(false);
        valork.setText("");
        calcularPk.setVisible(false);
        valorpLbl.setVisible(false);
        valorPolK.setVisible(false);

        //Selecciono las tuplas de la tabla puntos
        final ObservableList<Punto2D> tablaPuntoSeleccionado = tablaPuntos.getSelectionModel().getSelectedItems();
        tablaPuntoSeleccionado.addListener(selectorTablaPuntos);
    }
}
