package finter.modelo;

import java.util.ArrayList;

public class DesarrolloDeLagrange {
    private ArrayList<IndiceDeLagrange> indicesDeLagrange = new ArrayList<>();

    public String obtenerDetalle(){
        StringBuilder detalle = new StringBuilder();

        int indice = 0;
        for(IndiceDeLagrange indiceDeLagrange : indicesDeLagrange){
            detalle.append("L_").append(indice).append("=[").append(indiceDeLagrange.getNumerador()).append("]/[").append(indiceDeLagrange.getDenominador()).append("]\n");
            indice++;
        }

        return detalle.toString();
    }

    public String obtenerPolinomioDeLagrange(){
        StringBuilder polinomioDeLagrange = new StringBuilder();
        IndiceDeLagrange indice = this.getIndicesDeLagrange().get(0);
        polinomioDeLagrange.append(indice.getValorY()).append("*[").append(indice.getNumerador()).append("]/[").append(indice.getDenominador()).append("]");
        for (int i = 1; i < this.getIndicesDeLagrange().size(); i++) {
            indice = this.getIndicesDeLagrange().get(i);
            polinomioDeLagrange.append(" + ").append(indice.getValorY()).append("*[").append(indice.getNumerador()).append("]/[").append(indice.getDenominador()).append("]");
        }
        return polinomioDeLagrange.toString();
    }

    public ArrayList<IndiceDeLagrange> getIndicesDeLagrange() {
        return indicesDeLagrange;
    }

}
