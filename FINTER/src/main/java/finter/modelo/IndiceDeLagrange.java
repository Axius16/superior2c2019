package finter.modelo;

import java.util.Objects;

public class IndiceDeLagrange {

    private String numerador;
    private String denominador;
    private String valorY;

    public IndiceDeLagrange(String numerador, String denominador, String valorY) {
        this.numerador = numerador;
        this.denominador = denominador;
        this.valorY = valorY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndiceDeLagrange that = (IndiceDeLagrange) o;
        return Objects.equals(numerador, that.numerador) &&
                Objects.equals(denominador, that.denominador) &&
                Objects.equals(valorY, that.valorY);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numerador, denominador, valorY);
    }

    @Override
    public String toString() {
        return "IndiceDeLagrange{" +
                "numerador='" + numerador + '\'' +
                ", denominador='" + denominador + '\'' +
                ", valorY='" + valorY + '\'' +
                '}';
    }

    public String getValorY() {
        return valorY;
    }

    public void setValorY(String valorY) {
        this.valorY = valorY;
    }

    public String getNumerador() {
        return numerador;
    }

    public void setNumerador(String numerador) {
        this.numerador = numerador;
    }

    public String getDenominador() {
        return denominador;
    }

    public void setDenominador(String denominador) {
        this.denominador = denominador;
    }
}
