package finter.modelo;

import javafx.beans.property.SimpleDoubleProperty;
//Esta clase funciona de adaptador para la tabla
import javafx.util.Pair;
public class Punto2D {
	public SimpleDoubleProperty valorX; 
	public SimpleDoubleProperty valorY;

	
	public Punto2D() {
		
	}
	
	public Punto2D(Double valorX,Double valorY) {
		this.valorX= new SimpleDoubleProperty(valorX);
		this.valorY=new SimpleDoubleProperty(valorY);		
	}	
	
	
	public Double getValorX() {
		return valorX.get();
	}

	public Double getValorY() {
		return valorY.get();
	}
	
	public void setValorX(Double valorX) {
		this.valorX=new SimpleDoubleProperty(valorX);
	}
	
	public void setValorY(Double valorY) {
		this.valorY=new SimpleDoubleProperty(valorY);
	}

	

}
