package finter.modelo;

import javafx.util.Pair;
import org.apache.commons.text.TextStringBuilder;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DesarrolloDeNewtonGregory {
    private ArrayList<ArrayList<Double>> indicesDeNewtonGregory = new ArrayList<>();
    private ArrayList<Pair<Double, Double>> paresOrdenados;

    DesarrolloDeNewtonGregory(ArrayList<Pair<Double, Double>> paresOrdenados) {
        this.paresOrdenados = paresOrdenados;
    }

    public String obtenerDetalle() {
        TextStringBuilder detalle = new TextStringBuilder();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);

        Integer largoMaximo = obtenerLargoMaximo(df);

        List<String> valoresX = paresOrdenados.stream().map(par -> {
                    TextStringBuilder valor = new TextStringBuilder();
                    String numero = df.format(par.getKey());
                    return valor.appendFixedWidthPadRight(numero, largoMaximo, ' ').toString();
                }
        ).collect(Collectors.toList());
        List<String> valoresY = paresOrdenados.stream().map(par -> {
                    TextStringBuilder valor = new TextStringBuilder();
                    String numero = df.format(par.getValue());
                    return valor.appendFixedWidthPadRight(numero, largoMaximo, ' ').toString();
                }
        ).collect(Collectors.toList());
        detalle.appendFixedWidthPadLeft("X: ", 6, ' ').append(valoresX).append("\n");
        detalle.appendFixedWidthPadLeft("Y: ", 6, ' ').append(valoresY).append("\n");
        int indice = 0;
        for (ArrayList<Double> listaDoubles : indicesDeNewtonGregory) {
            indice++;
            List<String> indices = listaDoubles.stream().map(numeroDoble -> {
                        TextStringBuilder valor = new TextStringBuilder();
                        String numero = df.format(numeroDoble);
                        return valor.appendFixedWidthPadRight(numero, largoMaximo, ' ').toString();
                    }
            ).collect(Collectors.toList());
            detalle.appendFixedWidthPadLeft("d"+indice+": ", 6, ' ').append(indices).append("\n");
        }
        return detalle.toString();
    }

    private Integer obtenerLargoMaximo(DecimalFormat df) {
        List<Integer> largosDeIndices = new ArrayList<>();
        largosDeIndices.add(df.format(paresOrdenados.stream().max(Comparator.comparing(par -> df.format(par.getKey()).length())).get().getKey()).length());
        largosDeIndices.add(df.format(paresOrdenados.stream().max(Comparator.comparing(par -> df.format(par.getValue()).length())).get().getValue()).length());
        for (ArrayList<Double> listaDoubles : indicesDeNewtonGregory) {
            Integer largoIndice = df.format(listaDoubles.stream().max(Comparator.comparing(numeroDouble -> df.format(numeroDouble).length())).get()).length();
            largosDeIndices.add(largoIndice);
        }
        return largosDeIndices.stream().max(Integer::compareTo).get();
    }

    public ArrayList<ArrayList<Double>> getIndicesDeNewtonGregory() {
        return indicesDeNewtonGregory;
    }

    public String obtenerPolinomioProgresivo() {
        ArrayList<String> newtonGregoryProgresivo = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);

        newtonGregoryProgresivo.add(String.format("(%s)", df.format(paresOrdenados.get(0).getValue())).replace(",", "."));
        ArrayList<ArrayList<Double>> desarrolloNewtonGregory = this.getIndicesDeNewtonGregory();
        StringBuilder multiplicador = new StringBuilder();
        for (int i = 0; i < desarrolloNewtonGregory.size(); i++) {
            Double valor = paresOrdenados.get(i).getKey();
            String valorString = df.format(valor).replace(",", ".");

            if (valor.equals((double) 0)) {
                multiplicador.append("*X");
            } else if (valor.compareTo((double) 0) > 0) {
                multiplicador.append(String.format("*(X-%s)", valorString));
            } else {
                multiplicador.append(String.format("*(X%s)", valorString.replace("-", "+")));
            }
            newtonGregoryProgresivo.add(String.format("(%s)", df.format(desarrolloNewtonGregory.get(i).get(0)).replace(",", ".")) + multiplicador);
        }
        StringBuilder polinomioProgresivo = new StringBuilder(newtonGregoryProgresivo.get(0));
        for (int i = 1; i < newtonGregoryProgresivo.size(); i++) {
            polinomioProgresivo.append(" + ").append(newtonGregoryProgresivo.get(i));
        }
        return polinomioProgresivo.toString();
    }

    public String obtenerPolinomioRegresivo() {
        ArrayList<String> newtonGregoryRegresivo = new ArrayList<>();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);

        newtonGregoryRegresivo.add(String.format("(%s)", df.format(paresOrdenados.get(paresOrdenados.size() - 1).getValue()).replace(",", ".")));
        ArrayList<ArrayList<Double>> desarrolloNewtonGregory = this.getIndicesDeNewtonGregory();
        StringBuilder multiplicador = new StringBuilder();
        for (int i = 0; i < desarrolloNewtonGregory.size(); i++) {
            Double valor = paresOrdenados.get(paresOrdenados.size() - 1 - i).getKey();
            if (valor.equals((double) 0)) {
                multiplicador.append("*X");
            } else if (valor.compareTo((double) 0) > 0) {
                multiplicador.append(String.format("*(X-%s)", df.format(valor)).replace(",", "."));
            } else {
                multiplicador.append(String.format("*(X%s)", df.format(valor).replace("-", "+").replace(",", ".")));
            }
            newtonGregoryRegresivo.add(String.format("(%s)", df.format(desarrolloNewtonGregory.get(i).get(desarrolloNewtonGregory.get(i).size() - 1)).replace(",", ".")) + multiplicador);
        }
        StringBuilder polinomioRegresivo = new StringBuilder(newtonGregoryRegresivo.get(0));
        for (int i = 1; i < newtonGregoryRegresivo.size(); i++) {
            polinomioRegresivo.append(" + ").append(newtonGregoryRegresivo.get(i));
        }
        return polinomioRegresivo.toString();
    }
}
