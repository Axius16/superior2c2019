package finter.modelo;

import javafx.util.Pair;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Interpolador {
    private ArrayList<Pair<Double, Double>> paresOrdenados;
    private DesarrolloDeNewtonGregory desarrolloNewtonGregory;
    private DesarrolloDeLagrange desarrolloDeLagrange;

    public Interpolador(ArrayList<Pair<Double, Double>> paresOrdenados){
        this.paresOrdenados = paresOrdenados;
    }
    
    public DesarrolloDeNewtonGregory obtenerDesarrolloNewtonGregory() {

        if(desarrolloNewtonGregory == null) {
            ejecutarDesarrolloNewtonGregory();
        }
        return desarrolloNewtonGregory;
    }

    public DesarrolloDeLagrange obtenerDesarrolloLagrange() {
        if(desarrolloDeLagrange == null) {
            ejecutarDesarrolloLagrange();
        }
        return desarrolloDeLagrange;
    }

    public Boolean susPuntosSonEquidistantes() {
        ArrayList<Pair<Double,Double>> pares = this.paresOrdenados;

        for(int i = 0; i < pares.size() - 2; i++){
            Double primero = pares.get(i).getKey();
            Double segundo = pares.get(i + 1).getKey();
            Double tercero = pares.get(i + 2).getKey();
            Double resta1 = segundo - primero;
            String resta1String = resta1.toString().replace("-", "");
            Double resta2 = tercero - segundo;
            String resta2String = resta2.toString().replace("-", "");
            if(!resta1String.equals(resta2String)){
                return false;
            }
        }
        return true;
    }

    public Polinomio obtenerPolinomioInterpolante() {
        String polinomioNewtonGregory = this.obtenerDesarrolloNewtonGregory().obtenerPolinomioProgresivo();
        ArrayList<Double> coeficientes =  CalculadoraPolinomios.calcularPolinomioResultante(polinomioNewtonGregory);
        return new Polinomio(coeficientes);
    }

    private void ejecutarDesarrolloLagrange() {
        desarrolloDeLagrange = new DesarrolloDeLagrange();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);
        IndiceDeLagrange indice;

        for (int i = 0; i < paresOrdenados.size(); i++) {
            StringBuilder numerador = new StringBuilder();
            StringBuilder denominador = new StringBuilder();
            String valorY;
            for (int j = 0; j < paresOrdenados.size(); j++) {
                if(i!=j){
                    Double valorI = paresOrdenados.get(i).getKey();
                    Double valorJ = paresOrdenados.get(j).getKey();

                    String formatoNumerador = "(X-%s)";
                    String formatoDenominador = "(%s-%s)";
                    if(valorJ<0){
                        formatoNumerador = "(X+%s)";
                        formatoDenominador = "(%s+%s)";
                    }
                    numerador.append(String.format(formatoNumerador, df.format(Math.abs(valorJ))));
                    denominador.append(String.format(formatoDenominador, df.format(valorI), df.format(Math.abs(valorJ))));
                }
            }
            valorY = String.format("(%s)", df.format(paresOrdenados.get(i).getValue()));
            indice = new IndiceDeLagrange(numerador.toString(), denominador.toString(), valorY);
            desarrolloDeLagrange.getIndicesDeLagrange().add(indice);
        }
    }

    @SuppressWarnings("unchecked")
    private void ejecutarDesarrolloNewtonGregory() {
        desarrolloNewtonGregory = new DesarrolloDeNewtonGregory(paresOrdenados);

        ArrayList<Double> valoresUltimaIteracion = new ArrayList<>();
        ArrayList<Double> valoresIteracionActual = new ArrayList<>();
        for (Pair<Double, Double> par : paresOrdenados) {
            valoresUltimaIteracion.add(new Double(par.getValue().toString()));
        }

        for (int j = 0; j < paresOrdenados.size() - 1; j++) {

            for (int i = 0; i < valoresUltimaIteracion.size() - 1; i++) {

                Double baseActual = new Double(paresOrdenados.get(i).getKey().toString());
                Double baseSiguiente = new Double(paresOrdenados.get(1 + i +j).getKey().toString());
                Double valorActual = valoresUltimaIteracion.get(i);
                Double valorSiguiente = valoresUltimaIteracion.get(i + 1);

                Double resultado = (valorSiguiente - valorActual) / (baseSiguiente - baseActual);

                valoresIteracionActual.add(resultado);
            }
            this.obtenerDesarrolloNewtonGregory().getIndicesDeNewtonGregory().add((ArrayList<Double>) valoresIteracionActual.clone());
            if (valoresIteracionActual.stream().allMatch(valoresIteracionActual.get(0)::equals)) {
                break;
            }
            valoresUltimaIteracion = (ArrayList<Double>) valoresIteracionActual.clone();
            valoresIteracionActual.clear();
        }
    }
}
