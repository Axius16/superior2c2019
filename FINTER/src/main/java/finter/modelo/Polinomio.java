package finter.modelo;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;

public class Polinomio {
    private int grado;
    private ArrayList<Double> coeficientes;

    public Polinomio() {
        grado = 0;
        this.coeficientes = new ArrayList<>();

        coeficientes.add(new Double("0"));
    }

    public Polinomio(String coeficientes) {
        String[] array = coeficientes.split("\\s");
        Double k;
        this.coeficientes = new ArrayList<>();

        for (String anArray : array) {
            try {
                k = new Double(anArray);
                this.coeficientes.add(k);
            } catch (NumberFormatException e) {
                System.out.println("Se ha encontrado un valor No Real");
            }
        }
        grado = this.coeficientes.size() - 1;
    }

    public Polinomio(ArrayList<Double> coeficientes) {
        grado = coeficientes.size() - 1;
        this.coeficientes = new ArrayList<>();
        this.coeficientes.addAll(coeficientes);
    }

    public Polinomio(Polinomio polinomio) {
        grado = polinomio.grado;
        this.coeficientes = new ArrayList<>();
        this.coeficientes.addAll(polinomio.coeficientes);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Polinomio polinomio = (Polinomio) o;
        return grado == polinomio.grado &&
                Objects.equals(coeficientes, polinomio.coeficientes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(grado, coeficientes);
    }

    @Override
    public String toString() {
        StringBuilder string = new StringBuilder();
        DecimalFormat df = new DecimalFormat("#");
        df.setMaximumFractionDigits(6);
        df.setMinimumIntegerDigits(1);
        int pS = grado;

        if (pS == 0) {
            string = new StringBuilder(df.format(coeficientes.get(0)).replace(",", "."));
        } else {
            for (int i = pS; i >= 0; i--) {
                if (!coeficientes.get(i).equals(new Double("0"))) {
                    if (i != pS) {
                        string.append(" + ");
                    }
                    string.append("(").append(df.format(coeficientes.get(i)).replace(",", ".")).append(")");

                    switch (i) {
                        case 0:
                            break;
                        case 1:
                            string.append("*(X)");
                            break;
                        default:
                            string.append("*(X^").append(i).append(")");
                            break;
                    }
                }
            }
        }
        return string.toString();
    }

    public int getGrado() {
        return grado;
    }

    void setGrado(int grado) {
        this.grado = grado;
    }

    public ArrayList<Double> getCoeficientes() {
        return coeficientes;
    }
}
