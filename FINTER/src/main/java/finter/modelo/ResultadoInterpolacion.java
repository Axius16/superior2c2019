package finter.modelo;

public class ResultadoInterpolacion {

    private String polinomioResultante;
    private String polinomioExtendido;
    private String grado;
    private String losPuntosSonEquidistantes;
    private String desarrolloDelMetodoRealizado;
    private Polinomio polinomio;

    public String getPolinomioResultante() {
        return polinomioResultante;
    }

    public void setPolinomioResultante(String polinomioResultante) {
        this.polinomioResultante = polinomioResultante;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getLosPuntosSonEquidistantes() {
        return losPuntosSonEquidistantes;
    }

    public void setLosPuntosSonEquidistantes(String losPuntosSonEquidistantes) {
        this.losPuntosSonEquidistantes = losPuntosSonEquidistantes;
    }

    public String getDesarrolloDelMetodoRealizado() {
        return desarrolloDelMetodoRealizado;
    }

    public void setDesarrolloDelMetodoRealizado(String desarrolloDelMetodoRealizado) {
        this.desarrolloDelMetodoRealizado = desarrolloDelMetodoRealizado;
    }

    public String getPolinomioExtendido() {
        return polinomioExtendido;
    }

    public void setPolinomioExtendido(String polinomioExtendido) {
        this.polinomioExtendido = polinomioExtendido;
    }

    public Polinomio getPolinomio() {
        return polinomio;
    }

    public void setPolinomio(Polinomio polinomio) {
        this.polinomio = polinomio;
    }
}
