package finter.modelo;

import java.util.ArrayList;

public class CalculadoraPolinomios {

    public static final String equisConTerminoIndependiente = "^X[+-][0-9]+([.,][0-9]+)?$";
    public static final String soloTerminoIndependiente = "^[-]?[0-9]+([.,][0-9]+)?$";
    public static final String soloEquis = "^X$";

    public static ArrayList<Double> calcularPolinomioResultante(String polinomioNewtonGregory) {
        String[] valoresNewtonGregory = polinomioNewtonGregory.split(" \\+ ");

        ArrayList<Polinomio> polinomiosASumar = new ArrayList<>();
        for (String valorNewtonGregory : valoresNewtonGregory) {
            String[] polinomios = valorNewtonGregory.split("\\*");
            ArrayList<Polinomio> polinomiosAMultiplicar = new ArrayList<>();
            String coeficientes;
            for (String termino : polinomios) {
                termino = termino
                        .replace("(", "")
                        .replace(")", "");
                if (termino.matches(soloEquis)) {
                    //X
                    coeficientes = "0 1";
                } else if (termino.matches(equisConTerminoIndependiente)) {
                    //X+1
                    //X-1
                    termino = termino
                            .replace("+", "")
                            .replace("X", "");
                    coeficientes = termino + " 1";
                } else if (termino.matches(soloTerminoIndependiente)) {
                    //-1
                    //1
                    coeficientes = termino;
                } else {
                    throw new RuntimeException("Ocurrió un error al generar el PolinomioResultante");
                }
                polinomiosAMultiplicar.add(new Polinomio(coeficientes));
            }
            polinomiosASumar.add(multiplicar(polinomiosAMultiplicar));
        }
        return sumar(polinomiosASumar).getCoeficientes();
    }

    public static Polinomio sumar(ArrayList<Polinomio> polinomiosASumar) {
        if (polinomiosASumar.size() == 1) {
            return polinomiosASumar.get(0);
        }
        return polinomiosASumar.stream().reduce(
                CalculadoraPolinomios::sumar
        ).get();
    }

    private static Polinomio sumar(Polinomio a, Polinomio b) {
        Polinomio ad = new Polinomio();
        int aS = a.getCoeficientes().size();
        int bS = b.getCoeficientes().size();
        int max = Math.max(aS, bS);
        ad.setGrado(max - 1);
        ad.getCoeficientes().set(0, a.getCoeficientes().get(0) + b.getCoeficientes().get(0));
        for (int i = 1; i <= max; i++)
            ad.getCoeficientes().add(new Double("0"));
        for (int i = 1; i < aS; i++)
            ad.getCoeficientes().set(i, ad.getCoeficientes().get(i) + a.getCoeficientes().get(i));
        for (int i = 1; i < bS; i++)
            ad.getCoeficientes().set(i, ad.getCoeficientes().get(i) + b.getCoeficientes().get(i));
        int posicionUltimoCoeficiente = ad.getCoeficientes().size() - 1;
        while (!ad.getCoeficientes().isEmpty() && ad.getCoeficientes().get(posicionUltimoCoeficiente).equals((double) 0)) {
            ad.getCoeficientes().remove(posicionUltimoCoeficiente);
            posicionUltimoCoeficiente = ad.getCoeficientes().size() - 1;
        }
        ad.setGrado(ad.getCoeficientes().size() - 1);
        return ad;
    }

    public static Polinomio multiplicar(ArrayList<Polinomio> polinomiosAMultiplicar) {
        if (polinomiosAMultiplicar.size() == 1) {
            return polinomiosAMultiplicar.get(0);
        }
        return polinomiosAMultiplicar.stream().reduce(
                CalculadoraPolinomios::multiplicar
        ).get();
    }

    private static Polinomio multiplicar(Polinomio a, Polinomio b) {

        ArrayList<Double> coefA = a.getCoeficientes();
        ArrayList<Double> coefB = b.getCoeficientes();
        int m = coefA.size();
        int n = coefB.size();
        {
            int capacidadInicial = m + n - 1;
            ArrayList<Double> producto = new ArrayList<>(capacidadInicial);

            for (int i = 0; i < capacidadInicial; i++) {
                producto.add((double) 0);
            }
            for (int i = 0; i < m; i++) {
                for (int j = 0; j < n; j++) {
                    producto.set(i + j, producto.get(i + j) + (coefA.get(i) * coefB.get(j)));
                }
            }
            return new Polinomio(producto);
        }
    }

    public static Double calcularValorDeY(Polinomio polinomio, Double valorX) {
        double resultado = (double) 0;
        int gradoActual = 0;

        for (Double coeficiente : polinomio.getCoeficientes()) {
            resultado = resultado + (Math.pow(valorX, gradoActual) * coeficiente);
            gradoActual++;
        }

        return resultado;
    }
}


