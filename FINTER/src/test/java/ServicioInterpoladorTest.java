import finter.controlador.ServicioInterpolador;
import finter.modelo.Polinomio;
import finter.modelo.ResultadoInterpolacion;
import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class ServicioInterpoladorTest {

    @Test
    public void dadaUnaListaDeValoresIngresadosConXRepetidosValidaSiHayValoresXRepetidos(){
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(-1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));
        valoresIngresados.add(new Pair<>(5d, 2d));

        ServicioInterpolador servicio = new ServicioInterpolador();
        Boolean hayRepetidos = servicio.validarSiTieneValoresXRepetidos(valoresIngresados);

        Assert.assertTrue(hayRepetidos);
    }

    @Test
    public void dadaUnaListaDeValoresIngresadosSinXRepetidosValidaQueNoHayValoresXRepetidos(){
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(-1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));

        ServicioInterpolador servicio = new ServicioInterpolador();
        Boolean hayRepetidos = servicio.validarSiTieneValoresXRepetidos(valoresIngresados);

        Assert.assertFalse(hayRepetidos);
    }

    @Test
    public void dadoUnaListaDeValoresIngresadosYUnMetodoIngresadoRetornaUnResuladoInterpolacion(){
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));
        String metodoAUtilizar = "Lagrange";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, metodoAUtilizar);

        Assert.assertNotNull(resultado);
    }

    @Test
    public void dadoUnaListaDeValoresIngresadosYUnMetodoIngresadoRetornaUnResuladoInterpolacionConTodosSusAtributosCargados(){
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));
        String metodoAUtilizar = "Lagrange";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, metodoAUtilizar);

        Assert.assertNotNull(resultado.getDesarrolloDelMetodoRealizado());
        Assert.assertNotNull(resultado.getGrado());
        Assert.assertNotNull(resultado.getPolinomioResultante());
        Assert.assertNotNull(resultado.getPolinomioExtendido());
        Assert.assertNotNull(resultado.getLosPuntosSonEquidistantes());
    }

    @Test
    public void dadoUnaListaDeValoresYElMetodoDeNewtonGregoryProgresivoDevuelveLosValoresCorrectos(){
        String polinomioExtendidoEsperado = "(-0.104167)*(X^3) + (0.812503)*(X^2) + (-0.895841)*(X) + (-0.812495)";
        String gradoEsperado = "3";
        String losPuntosSonEquidistantesEsperado = "SI";
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));
        String desarrolloDelMetodoRealizadoEsperado =
                "   X: [1        , 3        , 5        , 7        ]\n" +
                "   Y: [-1       , 1        , 2        , -3       ]\n" +
                "  d1: [1        , 0.5      , -2.5     ]\n" +
                "  d2: [-0.125   , -0.75    ]\n" +
                "  d3: [-0.104167]\n";
        String polinomioResultanteEsperado = "(-1) + (1)*(X-1) + (-0.125)*(X-1)*(X-3) + (-0.104167)*(X-1)*(X-3)*(X-5)";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, "Newton Gregory Progresivo");

        Assert.assertEquals(gradoEsperado, resultado.getGrado());
        Assert.assertEquals(losPuntosSonEquidistantesEsperado, resultado.getLosPuntosSonEquidistantes());
        Assert.assertEquals(desarrolloDelMetodoRealizadoEsperado, resultado.getDesarrolloDelMetodoRealizado());
        Assert.assertEquals(polinomioExtendidoEsperado, resultado.getPolinomioExtendido());
        Assert.assertEquals(polinomioResultanteEsperado, resultado.getPolinomioResultante());
    }

    @Test
    public void dadoUnaListaDeValoresYElMetodoDeNewtonGregoryProgresivoConValoresConDecimalesDevuelveLosValoresCorrectos(){
        String polinomioExtendidoEsperado = "(-0.000074)*(X^3) + (0.069017)*(X^2) + (-12.319744)*(X) + (505.863849)";
        String gradoEsperado = "3";
        String losPuntosSonEquidistantesEsperado = "NO";
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(112.123123d, -112.123123));
        valoresIngresados.add(new Pair<>(30.123123d, 195.357856d));
        valoresIngresados.add(new Pair<>(522.951462d, 2354.587682d));
        valoresIngresados.add(new Pair<>(711.9519962d, -0.463413));
        String desarrolloDelMetodoRealizadoEsperado =
                "   X: [112.123123 , 30.123123  , 522.951462 , 711.951996 ]\n" +
                "   Y: [-112.123123, 195.357856 , 2354.587682, -0.463413  ]\n" +
                "  d1: [-3.749768  , 4.381302   , -12.460553 ]\n" +
                "  d2: [0.019792   , -0.024701  ]\n" +
                "  d3: [-0.000074  ]\n";
        String polinomioResultanteEsperado = "(-112.123123) + (-3.749768)*(X-112.123123) + (0.019792)*(X-112.123123)*(X-30.123123) + (-0.000074)*(X-112.123123)*(X-30.123123)*(X-522.951462)";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, "Newton Gregory Progresivo");

        Assert.assertEquals(gradoEsperado, resultado.getGrado());
        Assert.assertEquals(losPuntosSonEquidistantesEsperado, resultado.getLosPuntosSonEquidistantes());
        Assert.assertEquals(desarrolloDelMetodoRealizadoEsperado, resultado.getDesarrolloDelMetodoRealizado());
        Assert.assertEquals(polinomioResultanteEsperado, resultado.getPolinomioResultante());
        Assert.assertEquals(polinomioExtendidoEsperado, resultado.getPolinomioExtendido());
    }

    @Test
    public void dadoUnaListaDeValoresYElMetodoDeNewtonGregoryRegresivoDevuelveLosValoresCorrectos(){
        String polinomioExtendidoEsperado = "(-0.104167)*(X^3) + (0.812503)*(X^2) + (-0.895841)*(X) + (-0.812495)";
        String gradoEsperado = "3";
        String losPuntosSonEquidistantesEsperado = "SI";
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>(1d, (double) -1));
        valoresIngresados.add(new Pair<>(3d, 1d));
        valoresIngresados.add(new Pair<>(5d, 2d));
        valoresIngresados.add(new Pair<>(7d, (double) -3));
        String desarrolloDelMetodoRealizadoEsperado =
                "   X: [1        , 3        , 5        , 7        ]\n" +
                "   Y: [-1       , 1        , 2        , -3       ]\n" +
                "  d1: [1        , 0.5      , -2.5     ]\n" +
                "  d2: [-0.125   , -0.75    ]\n" +
                "  d3: [-0.104167]\n";
        String polinomioResultanteEsperado = "(-3) + (-2.5)*(X-7) + (-0.75)*(X-7)*(X-5) + (-0.104167)*(X-7)*(X-5)*(X-3)";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, "Newton Gregory Regresivo");

        Assert.assertEquals(polinomioExtendidoEsperado, resultado.getPolinomioExtendido());
        Assert.assertEquals(gradoEsperado, resultado.getGrado());
        Assert.assertEquals(losPuntosSonEquidistantesEsperado, resultado.getLosPuntosSonEquidistantes());
        Assert.assertEquals(desarrolloDelMetodoRealizadoEsperado, resultado.getDesarrolloDelMetodoRealizado());
        Assert.assertEquals(polinomioResultanteEsperado, resultado.getPolinomioResultante());
    }

    @Test
    public void dadoUnaListaDeValoresYElMetodoDeLagrangeDevuelveLosValoresCorrectos(){
        String polinomioExtendidoEsperado = "(-0.005026)*(X^6) + (0.045234)*(X^5) + (-0.12565)*(X^4) + (1.07539)*(X^3) + (-2.869324)*(X^2) + (0.879376)*(X) + (5)";
        Polinomio polinomioEsperado = new Polinomio("5.0 0.8793759999999999 -2.8693239999999998 1.07539 -0.12565 0.045233999999999996 -0.005026");
        String gradoEsperado = "6";
        String losPuntosSonEquidistantesEsperado = "NO";
        ArrayList<Pair<Double, Double>> valoresIngresados = new ArrayList<>();
        valoresIngresados.add(new Pair<>((double) -1, (double) 0));
        valoresIngresados.add(new Pair<>((double) 0, 5d));
        valoresIngresados.add(new Pair<>(1d, 4d));
        valoresIngresados.add(new Pair<>(2d, 3d));
        valoresIngresados.add(new Pair<>(3d, 8d));
        valoresIngresados.add(new Pair<>(4d, 25d));
        valoresIngresados.add(new Pair<>(8d, 29d));
        String desarrolloDelMetodoRealizadoEsperado =
                "L_0=[(X-0)(X-1)(X-2)(X-3)(X-4)(X-8)]/[(-1-0)(-1-1)(-1-2)(-1-3)(-1-4)(-1-8)]\n" +
                "L_1=[(X+1)(X-1)(X-2)(X-3)(X-4)(X-8)]/[(0+1)(0-1)(0-2)(0-3)(0-4)(0-8)]\n" +
                "L_2=[(X+1)(X-0)(X-2)(X-3)(X-4)(X-8)]/[(1+1)(1-0)(1-2)(1-3)(1-4)(1-8)]\n" +
                "L_3=[(X+1)(X-0)(X-1)(X-3)(X-4)(X-8)]/[(2+1)(2-0)(2-1)(2-3)(2-4)(2-8)]\n" +
                "L_4=[(X+1)(X-0)(X-1)(X-2)(X-4)(X-8)]/[(3+1)(3-0)(3-1)(3-2)(3-4)(3-8)]\n" +
                "L_5=[(X+1)(X-0)(X-1)(X-2)(X-3)(X-8)]/[(4+1)(4-0)(4-1)(4-2)(4-3)(4-8)]\n" +
                "L_6=[(X+1)(X-0)(X-1)(X-2)(X-3)(X-4)]/[(8+1)(8-0)(8-1)(8-2)(8-3)(8-4)]\n";
        String polinomioResultanteEsperado =
                "(0)*[(X-0)(X-1)(X-2)(X-3)(X-4)(X-8)]/[(-1-0)(-1-1)(-1-2)(-1-3)(-1-4)(-1-8)] + " +
                "(5)*[(X+1)(X-1)(X-2)(X-3)(X-4)(X-8)]/[(0+1)(0-1)(0-2)(0-3)(0-4)(0-8)] + " +
                "(4)*[(X+1)(X-0)(X-2)(X-3)(X-4)(X-8)]/[(1+1)(1-0)(1-2)(1-3)(1-4)(1-8)] + " +
                "(3)*[(X+1)(X-0)(X-1)(X-3)(X-4)(X-8)]/[(2+1)(2-0)(2-1)(2-3)(2-4)(2-8)] + " +
                "(8)*[(X+1)(X-0)(X-1)(X-2)(X-4)(X-8)]/[(3+1)(3-0)(3-1)(3-2)(3-4)(3-8)] + " +
                "(25)*[(X+1)(X-0)(X-1)(X-2)(X-3)(X-8)]/[(4+1)(4-0)(4-1)(4-2)(4-3)(4-8)] + " +
                "(29)*[(X+1)(X-0)(X-1)(X-2)(X-3)(X-4)]/[(8+1)(8-0)(8-1)(8-2)(8-3)(8-4)]";

        ServicioInterpolador servicio = new ServicioInterpolador();
        ResultadoInterpolacion resultado = servicio.ejecutarInterpolacion(valoresIngresados, "Lagrange");

        Assert.assertEquals(gradoEsperado, resultado.getGrado());
        Assert.assertEquals(losPuntosSonEquidistantesEsperado, resultado.getLosPuntosSonEquidistantes());
        Assert.assertEquals(desarrolloDelMetodoRealizadoEsperado, resultado.getDesarrolloDelMetodoRealizado());
        Assert.assertEquals(polinomioResultanteEsperado, resultado.getPolinomioResultante());
        Assert.assertEquals(polinomioEsperado, resultado.getPolinomio());
        Assert.assertEquals(polinomioExtendidoEsperado, resultado.getPolinomioExtendido());
    }

    @Test
    public void dadoUnPolinomioDeGradoCuatroYUnValorKDevuelveElValorCorrespondiente(){
        //23 X^4 - 120 X^3 + 62 X^2 + 208 X + 1
        Polinomio polinomio = new Polinomio("1 208 62 -120 23");
        Double valorK = -12.25;
        String valorCalculadoEsperado = "745280.089844";

        ServicioInterpolador servicio = new ServicioInterpolador();
        String valorCalculado = servicio.calcularValorEnK(polinomio, valorK);

        Assert.assertEquals(valorCalculadoEsperado, valorCalculado);
    }

    @Test
    public void dadoUnNuevoGrupoDeValoresValidaSiElPolinomioGeneradoEsIgualAlAnterior(){
        Polinomio polinomioAnterior = new Polinomio("-0.8124950000000002 -0.8958409999999999 0.812503 -0.104167");
        ArrayList<Pair<Double, Double>> nuevosValoresIngresados = new ArrayList<>();
        nuevosValoresIngresados.add(new Pair<>(1d, (double) -1));
        nuevosValoresIngresados.add(new Pair<>(3d, 1d));
        nuevosValoresIngresados.add(new Pair<>(5d, 2d));
        nuevosValoresIngresados.add(new Pair<>(7d, (double) -3));

        ServicioInterpolador servicio = new ServicioInterpolador();
        Boolean esIgualAlAnterior = servicio.esIgualAlAnterior(polinomioAnterior, nuevosValoresIngresados);

        Assert.assertTrue(esIgualAlAnterior);
    }
}
