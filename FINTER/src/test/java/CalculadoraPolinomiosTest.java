import finter.modelo.CalculadoraPolinomios;
import finter.modelo.Polinomio;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class CalculadoraPolinomiosTest {

    @Test
    public void dadoElStringDeUnNumeroPositivoSoloValidaConLaRegexCorrespondiente(){
        String numero = "12345.123";

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroPositivoConUnaComaSoloValidaConLaRegexCorrespondiente(){
        String numero = "12345,123";

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoUnDoublePositivoSoloValidaConLaRegexCorrespondiente(){
        String numero = new Double("1231231.3213").toString();

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroNegativoSoloValidaConLaRegexCorrespondiente(){
        String numero = "-12345.123123";

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoUnDoubleNegativoSoloValidaConLaRegexCorrespondiente(){
        String numero = new Double("-1231231.12312").toString();

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroSumadoPorUnaXSoloValidaConLaRegexCorrespondiente(){
        String numero = "X+12345.123";

        Assert.assertTrue(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroConUnaComaSumadoPorUnaXSoloValidaConLaRegexCorrespondiente(){
        String numero = "X+12345,123";

        Assert.assertTrue(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroRestadoPorUnaXSoloValidaConLaRegexCorrespondiente(){
        String numero = "X-12345.123123";

        Assert.assertTrue(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoUnaXSolaSoloValidaConLaRegexCorrespondiente(){
        String numero = "X";

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertTrue(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoElStringDeUnNumeroConDosPuntosNoValidaConNingunaRegex(){
        String numero = "-12345.123.123";

        Assert.assertFalse(numero.matches(CalculadoraPolinomios.equisConTerminoIndependiente));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloEquis));
        Assert.assertFalse(numero.matches(CalculadoraPolinomios.soloTerminoIndependiente));
    }

    @Test
    public void dadoUnDesarrolloDeUnPolinomioDeNewtonGregoryRetornaElPolinomioCorrecto(){
        String polinomioDeNewtonGregory = "(-2) + (3)*(X+1) + (11)*(X+1)*(X) + (-5)*(X+1)*(X)*(X-2) + (23)*(X+1)*(X)*(X-2)*(X-4)";
        //23 X^4 - 120 X^3 + 62 X^2 + 208 X + 1
        ArrayList<Double> polinomioEsperado = new ArrayList<>();
        polinomioEsperado.add(new Double("1"));
        polinomioEsperado.add(new Double("208"));
        polinomioEsperado.add(new Double("62"));
        polinomioEsperado.add(new Double("-120"));
        polinomioEsperado.add(new Double("23"));

        ArrayList<Double> polinomioCalculado = CalculadoraPolinomios.calcularPolinomioResultante(polinomioDeNewtonGregory);

        Assert.assertEquals(polinomioEsperado, polinomioCalculado);
    }

    @Test
    public void dadoUnPolinomioSinMultiplicarLoDevuelveMultiplicado(){
        //(x+1)(x-1)(x+2)(x-3) = x⁴-x³-7x²+x+6
        ArrayList<Double> coeficientesEsperados = new ArrayList<>();
        coeficientesEsperados.add(6d);
        coeficientesEsperados.add(1d);
        coeficientesEsperados.add((double) -7);
        coeficientesEsperados.add((double) -1);
        coeficientesEsperados.add(1d);
        ArrayList<Polinomio> polinomios = new ArrayList<>();
        polinomios.add(new Polinomio("1 1"));
        polinomios.add(new Polinomio("-1 1"));
        polinomios.add(new Polinomio("2 1"));
        polinomios.add(new Polinomio("-3 1"));

        Polinomio resultado = CalculadoraPolinomios.multiplicar(polinomios);

        Assert.assertArrayEquals(coeficientesEsperados.toArray(), resultado.getCoeficientes().toArray());
    }

    @Test
    public void dadoPolinomiosSinSumarLosDevuelveSumados(){
        //(X²-2X+1)+(X+1)+(X)+(-23)+(2X²-3)+(-2X²+4X-5)+(X⁴+12)+(X³-2X²-5X)+(-5X⁴-12)
        // = -4 X^4 + X^3 - X^2 - X - 29
        ArrayList<Double> coeficientesEsperados = new ArrayList<>();
        coeficientesEsperados.add((double) -29);
        coeficientesEsperados.add((double) -1);
        coeficientesEsperados.add((double) -1);
        coeficientesEsperados.add(1d);
        coeficientesEsperados.add((double) -4);
        ArrayList<Polinomio> polinomios = new ArrayList<>();
        polinomios.add(new Polinomio("1 -2 1"));
        polinomios.add(new Polinomio("1 1"));
        polinomios.add(new Polinomio("0 1"));
        polinomios.add(new Polinomio("-23"));
        polinomios.add(new Polinomio("-3 0 2"));
        polinomios.add(new Polinomio("-5 4 -2"));
        polinomios.add(new Polinomio("12 0 0 0 1"));
        polinomios.add(new Polinomio("0 -5 -2 1"));
        polinomios.add(new Polinomio("-12 0 0 0 -5"));

        Polinomio resultado = CalculadoraPolinomios.sumar(polinomios);

        Assert.assertArrayEquals(coeficientesEsperados.toArray(), resultado.getCoeficientes().toArray());
    }

    @Test
    public void dadoUnPolinomioDeGradoUnoYUnValorDeXCalculaElValorDeY(){
        ArrayList<Double> coeficientes = new ArrayList<>();
        coeficientes.add(2d);
        coeficientes.add(3d);
        Polinomio polinomio = new Polinomio(coeficientes);
        Double valorX = new Double("2");
        Double resultadoEsperado = new Double("8");

        Double resultado = CalculadoraPolinomios.calcularValorDeY(polinomio, valorX);

        Assert.assertEquals(resultadoEsperado, resultado);
    }

    @Test
    public void dadoUnPolinomioDeGradoDosYUnValorDeXCalculaElValorDeY(){
        ArrayList<Double> coeficientes = new ArrayList<>();
        coeficientes.add(2d);
        coeficientes.add(3d);
        coeficientes.add(4d);
        Polinomio polinomio = new Polinomio(coeficientes);
        Double valorX = new Double("2");
        Double resultadoEsperado = new Double("24");

        Double resultado = CalculadoraPolinomios.calcularValorDeY(polinomio, valorX);

        Assert.assertEquals(resultadoEsperado, resultado);
    }
}