import finter.modelo.IndiceDeLagrange;
import finter.modelo.Interpolador;
import finter.modelo.Polinomio;
import javafx.util.Pair;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class InterpoladorTest {


    @Test
    public void dadoDosParesDeValoresEquidistantesRetornaElPolinomioFinal(){
        Polinomio polinomioResultanteEsperado = new Polinomio("0 1");

        Interpolador polinomioInterpolador = new Interpolador(obtenerDosParesOrdenados());
        Polinomio polinomioResultante = polinomioInterpolador.obtenerPolinomioInterpolante();

        Assert.assertEquals(polinomioResultanteEsperado, polinomioResultante);
    }

    @Test
    public void dadoDosParesDeValoresDistanciadosRetornaElPolinomioFinal(){
        Polinomio polinomioResultanteEsperado = new Polinomio("-1 1.5");

        Interpolador polinomioInterpolador = new Interpolador(obtenerDosParesOrdenadosDistanciados());
        Polinomio polinomioResultante = polinomioInterpolador.obtenerPolinomioInterpolante();

        Assert.assertEquals(polinomioResultanteEsperado, polinomioResultante);
    }

    @Test
    public void dadoCincoParesDeValoresDeValoresElevadosRetornaElPolinomioFinal(){
        ArrayList<Double> coeficientes = new ArrayList<>();
        coeficientes.add(-0.1053130000000001);
        coeficientes.add(2.105807);
        coeficientes.add(-4.95E-4);
        coeficientes.add(1.0E-6);
        Polinomio polinomioResultanteEsperado = new Polinomio(coeficientes);

        Interpolador polinomioInterpolador = new Interpolador(obtenerCincoParesOrdenadosDeValoresElevados());
        Polinomio polinomioResultante = polinomioInterpolador.obtenerPolinomioInterpolante();

        Assert.assertEquals(polinomioResultanteEsperado, polinomioResultante);
    }

    @Test
    public void dadoTresParesDeValoresEquidistantesRetornaElDesarrolloDeUnPolinomioDeLagrange(){
        ArrayList<IndiceDeLagrange> desarrolloEsperado = new ArrayList<>();
        desarrolloEsperado.add(new IndiceDeLagrange("(X-2)(X-4)", "(1-2)(1-4)", "(1)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X-1)(X-4)", "(2-1)(2-4)", "(8)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X-1)(X-2)", "(4-1)(4-2)", "(64)"));

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerTresParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        ArrayList<IndiceDeLagrange> desarrolloLagrange = polinomioInterpolador.obtenerDesarrolloLagrange().getIndicesDeLagrange();

        Assert.assertEquals(desarrolloEsperado, desarrolloLagrange);
    }

    @Test
    public void dadoCuatroParesDeValoresEquidistantesRetornaElDesarrolloDeUnPolinomioDeLagrange(){
        ArrayList<IndiceDeLagrange> desarrolloEsperado = new ArrayList<>();
        desarrolloEsperado.add(new IndiceDeLagrange("(X-3)(X-5)(X-7)", "(1-3)(1-5)(1-7)", "(-1)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X-1)(X-5)(X-7)", "(3-1)(3-5)(3-7)", "(1)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X-1)(X-3)(X-7)", "(5-1)(5-3)(5-7)", "(2)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X-1)(X-3)(X-5)", "(7-1)(7-3)(7-5)", "(-3)"));

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerCuatroParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        ArrayList<IndiceDeLagrange> desarrolloLagrange = polinomioInterpolador.obtenerDesarrolloLagrange().getIndicesDeLagrange();

        Assert.assertEquals(desarrolloEsperado, desarrolloLagrange);
    }

    @Test
    public void dadoSeisParesDeValoresNoEquidistantesRetornaElDesarrolloDeUnPolinomioDeLagrange(){
        ArrayList<IndiceDeLagrange> desarrolloEsperado = new ArrayList<>();
        desarrolloEsperado.add(new IndiceDeLagrange("(X-0)(X-1)(X-2)(X-3)(X-4)(X-8)", "(-1-0)(-1-1)(-1-2)(-1-3)(-1-4)(-1-8)", "(0)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-1)(X-2)(X-3)(X-4)(X-8)", "(0+1)(0-1)(0-2)(0-3)(0-4)(0-8)", "(5)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-0)(X-2)(X-3)(X-4)(X-8)", "(1+1)(1-0)(1-2)(1-3)(1-4)(1-8)", "(4)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-0)(X-1)(X-3)(X-4)(X-8)", "(2+1)(2-0)(2-1)(2-3)(2-4)(2-8)", "(3)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-0)(X-1)(X-2)(X-4)(X-8)", "(3+1)(3-0)(3-1)(3-2)(3-4)(3-8)", "(8)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-0)(X-1)(X-2)(X-3)(X-8)", "(4+1)(4-0)(4-1)(4-2)(4-3)(4-8)", "(25)"));
        desarrolloEsperado.add(new IndiceDeLagrange("(X+1)(X-0)(X-1)(X-2)(X-3)(X-4)", "(8+1)(8-0)(8-1)(8-2)(8-3)(8-4)", "(29)"));

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerSeisParesOrdenadosNoEquidistantes();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        ArrayList<IndiceDeLagrange> desarrolloLagrange = polinomioInterpolador.obtenerDesarrolloLagrange().getIndicesDeLagrange();

        Assert.assertEquals(desarrolloEsperado, desarrolloLagrange);
    }

    @Test
    public void dadoCuatroParesDeValoresEquidistantesRetornaElDesarrolloDeUnPolinomioDeNewtonGregoryProgresivoDeGradoTres(){
        String polinomioDeNewtonGregoryEsperado = "(-18) + (16)*(X+2) + (-4)*(X+2)*(X+1) + (1)*(X+2)*(X+1)*(X-1)";
        int gradoEsperado = 3;

        Interpolador polinomioInterpolador = new Interpolador(obtenerCuatroParesOrdenadosConNegativos());
        String newtonGregoryProgresivo = polinomioInterpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioProgresivo();
        int grado = polinomioInterpolador.obtenerPolinomioInterpolante().getGrado();

        Assert.assertEquals(polinomioDeNewtonGregoryEsperado, newtonGregoryProgresivo);
        Assert.assertEquals(gradoEsperado, grado);
    }

    @Test
    public void dadoCincoParesDeValoresEquidistantesRetornaElDesarrolloDeUnPolinomioDeNewtonGregoryProgresivoDeGradoDos(){
        String polinomioDeNewtonGregoryEsperado = "(0) + (1)*X + (1)*X*(X-1)";
        int gradoEsperado = 2;

        Interpolador polinomioInterpolador = new Interpolador(obtenerCincoParesOrdenados());
        String newtonGregoryProgresivo = polinomioInterpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioProgresivo();
        int grado = polinomioInterpolador.obtenerPolinomioInterpolante().getGrado();

        Assert.assertEquals(polinomioDeNewtonGregoryEsperado, newtonGregoryProgresivo);
        Assert.assertEquals(gradoEsperado, grado);
    }

    @Test
    public void dadoSeisParesDeValoresNoEquidistantesRetornaElDesarrolloDeUnPolinomioDeNewtonGregoryProgresivoDeGradoTres(){
        String polinomioDeNewtonGregoryEsperado = "(0) + (5)*(X+1) + (-3)*(X+1)*X + (1)*(X+1)*X*(X-1)";
        int gradoEsperado = 3;

        Interpolador polinomioInterpolador = new Interpolador(obtenerSeisParesOrdenados());
        String newtonGregoryProgresivo = polinomioInterpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioProgresivo();
        int grado = polinomioInterpolador.obtenerPolinomioInterpolante().getGrado();

        Assert.assertEquals(polinomioDeNewtonGregoryEsperado, newtonGregoryProgresivo);
        Assert.assertEquals(gradoEsperado, grado);
    }

    @Test
    public void dadoCincoParesDeValoresEquidistantesRetornaElDesarrolloDeUnPolinomioDeNewtonGregoryRegresivoDeGradoDos(){
        int gradoEsperado = 2;
        String polinomioDeNewtonGregoryEsperado = "(16) + (7)*(X-4) + (1)*(X-4)*(X-3)";

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerCincoParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        String newtonGregoryRegresivo = polinomioInterpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioRegresivo();
        int grado = polinomioInterpolador.obtenerPolinomioInterpolante().getGrado();

        Assert.assertEquals(polinomioDeNewtonGregoryEsperado, newtonGregoryRegresivo);
        Assert.assertEquals(gradoEsperado, grado);
    }

    @Test
    public void dadoSeisParesDeValoresNoEquidistantesRetornaElDesarrolloDeUnPolinomioDeNewtonGregoryRegresivoDeGradoTres(){
        int gradoEsperado = 3;
        String polinomioDeNewtonGregoryEsperado = "(25) + (17)*(X-4) + (6)*(X-4)*(X-3) + (1)*(X-4)*(X-3)*(X-2)";

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerSeisParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        String newtonGregoryRegresivo = polinomioInterpolador.obtenerDesarrolloNewtonGregory().obtenerPolinomioRegresivo();
        int grado = polinomioInterpolador.obtenerPolinomioInterpolante().getGrado();

        Assert.assertEquals(polinomioDeNewtonGregoryEsperado, newtonGregoryRegresivo);
        Assert.assertEquals(gradoEsperado, grado);
    }

    @Test
    public void dadoCincoParesDeValoresEquidistantesRetornaTodosLosPasosParaRecuperarUnPolinomioDeNewtonGregoryDeGradoDos(){
        int columnasEsperadas = 2;
        ArrayList<ArrayList<Double>> valoresEsperados = new ArrayList<>();
        ArrayList<Double> primerColumna = new ArrayList<>();
        primerColumna.add(1d);
        primerColumna.add(3d);
        primerColumna.add(5d);
        primerColumna.add(7d);
        valoresEsperados.add(primerColumna);
        ArrayList<Double> segundaColumna = new ArrayList<>();
        segundaColumna.add(1d);
        segundaColumna.add(1d);
        segundaColumna.add(1d);
        valoresEsperados.add(segundaColumna);

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerCincoParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        ArrayList<ArrayList<Double>> desarrolloNewtonGregory = polinomioInterpolador.obtenerDesarrolloNewtonGregory().getIndicesDeNewtonGregory();
        int columnas = desarrolloNewtonGregory.size();

        Assert.assertEquals(valoresEsperados, desarrolloNewtonGregory);
        Assert.assertEquals(columnasEsperadas, columnas);
    }

    @Test
    public void dadoSeisParesDeValoresNoEquidistantesTodosLosPasosParaRecuperarUnPolinomioDeNewtonGregoryDeGradoTres(){
        int columnasEsperadas = 3;
        ArrayList<ArrayList<Double>> valoresEsperados = new ArrayList<>();
        ArrayList<Double> primerColumna = new ArrayList<>();
        primerColumna.add(5d);
        primerColumna.add((double) -1);
        primerColumna.add((double) -1);
        primerColumna.add(5d);
        primerColumna.add(17d);
        valoresEsperados.add(primerColumna);
        ArrayList<Double> segundaColumna = new ArrayList<>();
        segundaColumna.add((double) -3);
        segundaColumna.add((double) 0);
        segundaColumna.add(3d);
        segundaColumna.add(6d);
        valoresEsperados.add(segundaColumna);
        ArrayList<Double> tercerColumna = new ArrayList<>();
        tercerColumna.add(1d);
        tercerColumna.add(1d);
        tercerColumna.add(1d);
        valoresEsperados.add(tercerColumna);

        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerSeisParesOrdenados();

        Interpolador polinomioInterpolador = new Interpolador(paresOrdenados);
        ArrayList<ArrayList<Double>> desarrolloNewtonGregory = polinomioInterpolador.obtenerDesarrolloNewtonGregory().getIndicesDeNewtonGregory();
        int columnas = desarrolloNewtonGregory.size();

        Assert.assertEquals(valoresEsperados, desarrolloNewtonGregory);
        Assert.assertEquals(columnasEsperadas, columnas);
    }

    @Test
    public void dadosSeisPuntosNoEquidistantesFallaAlValidarSiSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerSeisParesOrdenadosNoEquidistantes();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertFalse(sonEquidistantes);
    }

    @Test
    public void dadosCuatroPuntosEquidistantesValidarQueSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerCuatroParesOrdenadosEquidistantesConDistanciaDeDos();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertTrue(sonEquidistantes);
    }

    @Test
    public void dadosSeisPuntosEquidistantesValidaSiSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerSeisParesOrdenados();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertTrue(sonEquidistantes);
    }

    @Test
    public void dadosTresPuntosNoEquidistantesFallaAlValidarSiSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerTresParesOrdenados();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertFalse(sonEquidistantes);
    }

    @Test
    public void dadosTresPuntosEquidistantesValidaSiSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerTresParesOrdenadosEquidistantes();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertTrue(sonEquidistantes);
    }

    @Test
    public void dadosDosPuntosValidaSiSonEquidistantes(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerDosParesOrdenados();

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Boolean sonEquidistantes = interpolador.susPuntosSonEquidistantes();

        Assert.assertTrue(sonEquidistantes);
    }

    @Test
    public void dadosTresPuntosObtengoElPoliniomioInterpolante(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerTresParesOrdenados();
        Polinomio polinomioEsperado = new Polinomio("8 -14 7");

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Polinomio polinomioInterpolante = interpolador.obtenerPolinomioInterpolante();

        Assert.assertNotNull(polinomioInterpolante.toString());
        Assert.assertEquals(polinomioEsperado, polinomioInterpolante);
    }

    @Test
    public void dadosTresPuntosConDivisionIrracionalObtengoElPoliniomioInterpolante(){
        ArrayList<Pair<Double, Double>> paresOrdenados = obtenerTresParesOrdenadosConDivisionIrracional();
        Polinomio polinomioEsperado = new Polinomio("23 -14.166667 2.166667");

        Interpolador interpolador = new Interpolador(paresOrdenados);
        Polinomio polinomioInterpolante = interpolador.obtenerPolinomioInterpolante();

        Assert.assertNotNull(polinomioInterpolante.toString());
        Assert.assertEquals(polinomioEsperado, polinomioInterpolante);
    }

    private ArrayList<Pair<Double, Double>> obtenerTresParesOrdenadosConDivisionIrracional() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>((double) 0, 23d));
        paresOrdenados.add(new Pair<>(1d, 11d));
        paresOrdenados.add(new Pair<>(4d, 1d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerDosParesOrdenadosDistanciados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(2d, 2d));
        paresOrdenados.add(new Pair<>(6d, 8d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerDosParesOrdenados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(1d, 1d));
        paresOrdenados.add(new Pair<>(2d, 2d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerTresParesOrdenados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(1d, 1d));
        paresOrdenados.add(new Pair<>(2d, 8d));
        paresOrdenados.add(new Pair<>(4d, 64d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerTresParesOrdenadosEquidistantes() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(1d, 1d));
        paresOrdenados.add(new Pair<>(2d, 8d));
        paresOrdenados.add(new Pair<>(3d, 64d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerCuatroParesOrdenados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(1d, (double) -1));
        paresOrdenados.add(new Pair<>(3d, 1d));
        paresOrdenados.add(new Pair<>(5d, 2d));
        paresOrdenados.add(new Pair<>(7d, (double) -3));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerCuatroParesOrdenadosConNegativos() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(-2d, -18d));
        paresOrdenados.add(new Pair<>(-1d, -2d));
        paresOrdenados.add(new Pair<>(1d, 6d));
        paresOrdenados.add(new Pair<>(2d, 10d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerCincoParesOrdenados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>((double) 0, (double) 0));
        paresOrdenados.add(new Pair<>(1d, 1d));
        paresOrdenados.add(new Pair<>(2d, 4d));
        paresOrdenados.add(new Pair<>(3d, 9d));
        paresOrdenados.add(new Pair<>(4d, 16d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerSeisParesOrdenados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>((double) -1, (double) 0));
        paresOrdenados.add(new Pair<>((double) 0, 5d));
        paresOrdenados.add(new Pair<>(1d, 4d));
        paresOrdenados.add(new Pair<>(2d, 3d));
        paresOrdenados.add(new Pair<>(3d, 8d));
        paresOrdenados.add(new Pair<>(4d, 25d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerSeisParesOrdenadosNoEquidistantes() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>((double) -1, (double) 0));
        paresOrdenados.add(new Pair<>((double) 0, 5d));
        paresOrdenados.add(new Pair<>(1d, 4d));
        paresOrdenados.add(new Pair<>(2d, 3d));
        paresOrdenados.add(new Pair<>(3d, 8d));
        paresOrdenados.add(new Pair<>(4d, 25d));
        paresOrdenados.add(new Pair<>(8d, 29d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerCuatroParesOrdenadosEquidistantesConDistanciaDeDos() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>((double) -3, -12.33));
        paresOrdenados.add(new Pair<>((double) -1, -5.67));
        paresOrdenados.add(new Pair<>(1d, 5.556d));
        paresOrdenados.add(new Pair<>(3d, 29d));
        return paresOrdenados;
    }

    private ArrayList<Pair<Double, Double>> obtenerCincoParesOrdenadosDeValoresElevados() {
        ArrayList<Pair<Double, Double>> paresOrdenados = new ArrayList<>();
        paresOrdenados.add(new Pair<>(1d, 2d));
        paresOrdenados.add(new Pair<>(11d, 23d));
        paresOrdenados.add(new Pair<>(114d, 235d));
        paresOrdenados.add(new Pair<>(1234d, 3453d));
        paresOrdenados.add(new Pair<>(12341d, 34535d));
        return paresOrdenados;
    }
}
